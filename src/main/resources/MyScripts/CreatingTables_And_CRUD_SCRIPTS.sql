-- === creating tables starts ===
CREATE TABLE customer_tb
(
    customer_id      serial       NOT NULL PRIMARY KEY,
    customer_name    varchar(100) NOT NULL,
    customer_address varchar(100) NOT NULL,
    customer_phone   varchar(20)  NOT NULL
);


CREATE TABLE invoice_tb
(
    invoice_id   serial    NOT NULL PRIMARY KEY,
    invoice_date timestamp NOT NULL DEFAULT NOW(),
    customer_id  int       NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES customer_tb (customer_id) ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE product_tb
(
    product_id    serial         NOT NULL PRIMARY KEY,
    product_name  varchar(100)   NOT NULL,
    product_price decimal(15, 2) NOT NULL
);



CREATE TABLE invoice_detail_tb
(
    id         serial PRIMARY KEY NOT NULL,
    invoice_id INT                NOT NULL,
    product_id INT                NOT NULL,
--     PRIMARY KEY (invoice_id, product_id),
    FOREIGN KEY (invoice_id) REFERENCES invoice_tb (invoice_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (product_id) REFERENCES product_tb (product_id) ON DELETE CASCADE ON UPDATE CASCADE
)

-- === creating tables ends ===













-- === CRUD testing ===

-- Product
SELECT *
FROM product_tb;

SELECT *
FROM product_tb
WHERE product_id = 1;

INSERT INTO product_tb (product_name, product_price)
VALUES ('Corona', 30)
RETURNING *;

UPDATE product_tb
SET product_name  = 'Jek',
    product_price = 10
WHERE product_id = 1
RETURNING *;

DELETE
FROM product_tb
WHERE product_id = 1;

-- Customer

SELECT *
FROM customer_tb;

SELECT *
FROM customer_tb
WHERE customer_id = 1;

INSERT INTO customer_tb (customer_name, customer_address, customer_phone)
VALUES ('Sithov', 'PP', '081323127'),
       ('Miles', 'PP', '081323127'),
       ('Morales', 'PP', '081323127')
RETURNING *;

UPDATE customer_tb
SET customer_name  = 'Vannda',
    customer_address = 'KPS',
    customer_phone = '012345678'
WHERE customer_id = 1
RETURNING *;

DELETE
FROM customer_tb
WHERE customer_id = 1;

-- Invoice

SELECT pt.product_id, pt.product_name, pt.product_price
FROM product_tb pt
         INNER JOIN invoice_detail_tb pdt ON pt.product_id = pdt.product_id
WHERE invoice_id = 10;

SELECT * FROM invoice_tb WHERE invoice_id = 1;

-- testing

INSERT INTO customer_tb (customer_name, customer_address, customer_phone)
VALUES ('Sithov', 'PP', '081323127'),
       ('Miles', 'NY', '099999999'),
       ('Morales', 'LA', '0888888888');

INSERT INTO product_tb (product_name, product_price)
VALUES ('Apple', 10),
       ('Banana', 20),
       ('Orange', 30),
       ('Mango', 40);

INSERT INTO invoice_tb (customer_id)
VALUES (2),
       (3),
       (2),
       (3);

INSERT INTO invoice_detail_tb (invoice_id, product_id)
VALUES (7, 2),
       (8, 2);