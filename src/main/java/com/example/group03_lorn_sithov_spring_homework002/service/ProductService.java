package com.example.group03_lorn_sithov_spring_homework002.service;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    List<Product> getAllProduct();

    Product getProductById(Integer id);

    Product addNewProduct(Product product);

    Product updateProduct(Product product, Integer id);

    Product deleteProduct(Integer id);
}
