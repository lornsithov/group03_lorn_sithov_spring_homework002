package com.example.group03_lorn_sithov_spring_homework002.service;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Invoice;
import com.example.group03_lorn_sithov_spring_homework002.model.request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {

    List<Invoice> getAllInvoice();

    Invoice getInvoiceById(Integer id);

    void insertInvoice(InvoiceRequest invoiceRequest);

    Invoice deleteInvoiceById(Integer id);

    void updateInvoiceById(InvoiceRequest invoiceRequest, Integer id);
}
