package com.example.group03_lorn_sithov_spring_homework002.service.ServiceImp;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Product;
import com.example.group03_lorn_sithov_spring_homework002.repository.ProductRepository;
import com.example.group03_lorn_sithov_spring_homework002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Product addNewProduct(Product product) {
        return productRepository.addNewProduct(product);
    }

    @Override
    public Product updateProduct(Product product, Integer id) {
        return productRepository.updateProduct(product, id);
    }

    @Override
    public Product deleteProduct(Integer id) {
        return productRepository.deleteProduct(id);
    }

}
