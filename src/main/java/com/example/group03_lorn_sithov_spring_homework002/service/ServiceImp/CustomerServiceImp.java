package com.example.group03_lorn_sithov_spring_homework002.service.ServiceImp;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Customer;
import com.example.group03_lorn_sithov_spring_homework002.repository.CustomerRepository;
import com.example.group03_lorn_sithov_spring_homework002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Customer addNewCustomer(Customer customer) {
        return customerRepository.addNewCustomer(customer);
    }

    @Override
    public Customer updateCustomer(Customer customer, Integer id) {
        return customerRepository.updateCustomer(customer, id);
    }

    @Override
    public Customer deleteCustomer(Integer id) {
        return customerRepository.deleteCustomer(id);
    }

}
