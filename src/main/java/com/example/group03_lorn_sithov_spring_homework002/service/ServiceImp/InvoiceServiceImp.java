package com.example.group03_lorn_sithov_spring_homework002.service.ServiceImp;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Invoice;
import com.example.group03_lorn_sithov_spring_homework002.model.request.InvoiceRequest;
import com.example.group03_lorn_sithov_spring_homework002.repository.InvoiceRepository;
import com.example.group03_lorn_sithov_spring_homework002.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {

    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.getAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer id) {
        return invoiceRepository.getInvoiceById(id);
    }

    @Override
    public void insertInvoice(InvoiceRequest invoiceRequest) {
        Integer invoiceId = invoiceRepository.insertInvoice(invoiceRequest);
        for (Integer productId : invoiceRequest.getProductId()) {
            invoiceRepository.insertIntoProductDetail(invoiceId, productId);
        }
    }

    @Override
    public void updateInvoiceById(InvoiceRequest invoiceRequest, Integer id) {
//        return invoiceRepository.updateInvoiceById(invoiceRequest, id);
        Integer invoiceId = invoiceRepository.updateInvoiceById(invoiceRequest, id);
        invoiceRepository.deleteIntoProductDetail(invoiceId);
        for (Integer productId : invoiceRequest.getProductId()) {
            invoiceRepository.insertUpdateIntoProductDetail(invoiceId, productId);
        }
    }

    @Override
    public Invoice deleteInvoiceById(Integer id) {
        return invoiceRepository.deleteInvoiceById(id);
    }

}
