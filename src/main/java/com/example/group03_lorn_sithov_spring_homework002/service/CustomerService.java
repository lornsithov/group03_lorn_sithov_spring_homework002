package com.example.group03_lorn_sithov_spring_homework002.service;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Customer;
import com.example.group03_lorn_sithov_spring_homework002.model.entity.Invoice;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomer();

    Customer getCustomerById(Integer id);

    Customer addNewCustomer(Customer customer);

    Customer updateCustomer(Customer customer, Integer id);

    Customer deleteCustomer(Integer id);

}
