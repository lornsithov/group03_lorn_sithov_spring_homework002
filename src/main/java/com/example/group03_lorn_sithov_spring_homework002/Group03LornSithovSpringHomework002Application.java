package com.example.group03_lorn_sithov_spring_homework002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Group03LornSithovSpringHomework002Application {

	public static void main(String[] args) {
		SpringApplication.run(Group03LornSithovSpringHomework002Application.class, args);
		System.out.println("successfully");
	}

}
