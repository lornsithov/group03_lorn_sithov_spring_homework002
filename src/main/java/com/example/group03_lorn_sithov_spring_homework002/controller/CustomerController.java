package com.example.group03_lorn_sithov_spring_homework002.controller;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Customer;
import com.example.group03_lorn_sithov_spring_homework002.model.response.CustomerResponse;
import com.example.group03_lorn_sithov_spring_homework002.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
    private final CustomerService customerService;
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get-all-customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer() {
        return ResponseEntity.ok(new CustomerResponse<>(
                customerService.getAllCustomer(),
                true,
                "successfully showing ALL customers..."
        ));
    }

    @GetMapping("/get-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable Integer id) {
        return ResponseEntity.ok(new CustomerResponse<>(
                customerService.getCustomerById(id),
                true,
                "successfully getting this customer."
        ));
    }

    @PostMapping("/add-new-customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody Customer customer) {
        return ResponseEntity.ok(new CustomerResponse<>(
                customerService.addNewCustomer(customer),
                true,
                "This customer has been ADDED successfully!"
        ));
    }

    @PutMapping("/update-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomer(@RequestBody Customer customer, @PathVariable Integer id) {
        return ResponseEntity.ok(new CustomerResponse<>(
                customerService.updateCustomer(customer, id),
                true,
                "This customer has been UPDATED successfully!"
        ));
    }

    @DeleteMapping("/delete-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<Customer>> deleteCustomer(@PathVariable Integer id) {
        return ResponseEntity.ok(new CustomerResponse<>(
                customerService.deleteCustomer(id),
                true,
                "Thee customer has been DELETED successfully!"
        ));
    }


}
