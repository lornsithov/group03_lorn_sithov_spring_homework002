package com.example.group03_lorn_sithov_spring_homework002.controller;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Invoice;
import com.example.group03_lorn_sithov_spring_homework002.model.request.InvoiceRequest;
import com.example.group03_lorn_sithov_spring_homework002.model.response.InvoiceResponse;
import com.example.group03_lorn_sithov_spring_homework002.service.InvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/get-all-invoice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice() {
        return ResponseEntity.ok(new InvoiceResponse<>(
                invoiceService.getAllInvoice(),
                true,
                "GETTING ALL the invoices successfully!"
        ));
    }

    @GetMapping("/get-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceById(@PathVariable Integer id) {
        return ResponseEntity.ok(new InvoiceResponse<>(
                invoiceService.getInvoiceById(id),
                true,
                "successfully GETTING this customer by ID"
        ));
    }

    @PostMapping("/add-new-invoice")
    public ResponseEntity<InvoiceResponse<Invoice>> insertInvoice(@RequestBody InvoiceRequest invoiceRequest) {
        invoiceService.insertInvoice(invoiceRequest);
        return ResponseEntity.ok(new InvoiceResponse<>(
                null,
                true,
                "this invoice has been ADDED successfully!"
        ));
    }

    @PutMapping("/update-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<Invoice>> updateInvoiceById(@RequestBody InvoiceRequest invoiceRequest ,@PathVariable Integer id) {
        invoiceService.updateInvoiceById(invoiceRequest,id);
        return ResponseEntity.ok(new InvoiceResponse<>(
                null,
                true,
                "this invoice has been UPDATED successfully!"
        ));
    }

    @DeleteMapping("/delete-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<Invoice>> deleteInvoiceById(@PathVariable Integer id) {
        return ResponseEntity.ok(new InvoiceResponse<>(
                invoiceService.deleteInvoiceById(id),
                true,
                "this invoice has been DELETED successfully!"
        ));
    }

}
