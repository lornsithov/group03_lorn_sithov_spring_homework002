package com.example.group03_lorn_sithov_spring_homework002.controller;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Product;
import com.example.group03_lorn_sithov_spring_homework002.model.response.ProductResponse;
import com.example.group03_lorn_sithov_spring_homework002.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping ("/get-all-product")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct() {
        return ResponseEntity.ok(new ProductResponse<>(
                productService.getAllProduct(),
                true,
                "successfully showing ALL products..."
        ));
    }

    @GetMapping("get-product-by-id/{id}")
    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable Integer id) {
        return ResponseEntity.ok(new ProductResponse<>(
                productService.getProductById(id),
                true,
                "successfully getting this product."
        ));
    }

    @PostMapping("add-new-product")
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody Product product) {
        return ResponseEntity.ok(new ProductResponse<>(
                productService.addNewProduct(product),
                true,
                "This product has been ADDED successfully!"
        ));
    }

    @PutMapping("update-product/{id}")
    public ResponseEntity<ProductResponse<Product>> updateProduct(@RequestBody Product product, @PathVariable Integer id) {
        return ResponseEntity.ok(new ProductResponse<>(
                productService.updateProduct(product, id),
                true,
                "This product has been UPDATED successfully!"
        ));
    }

    @DeleteMapping("delete-product/{id}")
    public ResponseEntity<ProductResponse<Product>> deleteProduct(@PathVariable Integer id) {
        return ResponseEntity.ok(new ProductResponse<>(
                productService.deleteProduct(id),
                true,
                "The product has been DELETED successfully!"
        ));
    }
}
