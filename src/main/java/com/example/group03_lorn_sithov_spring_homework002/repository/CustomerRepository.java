package com.example.group03_lorn_sithov_spring_homework002.repository;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Customer;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
@Repository
public interface CustomerRepository {

    @Select("""
            SELECT *
            FROM customer_tb;
            """)
    @Results(id = "customerMap", value = {
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "customerName", column = "customer_name"),
            @Result(property = "customerAddress", column = "customer_address"),
            @Result(property = "customerPhone", column = "customer_phone")
    })
    List<Customer> getAllCustomer();

    @Select("""
            SELECT *
            FROM customer_tb
            WHERE customer_id = #{id};
            """)
    @ResultMap("customerMap")
    Customer getCustomerById(Integer id);

    @Select("""
            INSERT INTO customer_tb (customer_name, customer_address, customer_phone)
            VALUES (#{customer.customerName}, #{customer.customerAddress}, #{customer.customerPhone})
            RETURNING *;
            """)
    @ResultMap("customerMap")
    Customer addNewCustomer(@Param("customer") Customer customer);

    @Select("""
            UPDATE customer_tb
            SET customer_name  = #{customer.customerName},
                customer_address = #{customer.customerAddress},
                customer_phone = #{customer.customerPhone}
            WHERE customer_id = #{id}
            RETURNING *;
            """)
    @ResultMap("customerMap")
    Customer updateCustomer(@Param("customer") Customer customer, @PathVariable Integer id);

    @Select("""
            DELETE
            FROM customer_tb
            WHERE customer_id = #{id};
            """)
    @ResultMap("customerMap")
    Customer deleteCustomer(Integer id);
}
