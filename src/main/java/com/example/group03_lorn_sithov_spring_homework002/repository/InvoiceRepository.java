package com.example.group03_lorn_sithov_spring_homework002.repository;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Invoice;
import com.example.group03_lorn_sithov_spring_homework002.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
@Repository
public interface InvoiceRepository {

    // ========== GET ALL INVOICES ==========
    @Select("""
            SELECT * FROM invoice_tb;
            """)
    @Results(id = "InvoiceMap", value = {
            @Result(property = "invoiceId", column = "invoice_id"),
            @Result(property = "invoiceDate", column = "invoice_date"),
            @Result(property = "customerId", column = "customer_id",
                    one = @One(select = "com.example.group03_lorn_sithov_spring_homework002.repository.CustomerRepository.getCustomerById")
            ),
            @Result(property = "products", column = "invoice_id",
                    many = @Many(select = "com.example.group03_lorn_sithov_spring_homework002.repository.ProductRepository.getAllInvoiceById")
            )
    })
    List<Invoice> getAllInvoice();


    // ========== GET INVOICE BY ID ==========
    @Select("""
            SELECT * FROM invoice_tb WHERE invoice_id = #{id};
            """)
    @ResultMap("InvoiceMap")
    Invoice getInvoiceById(@PathVariable Integer id);

    // ========== UPDATE ==========
    @Select("""
            UPDATE invoice_tb
            SET customer_id = #{invoiceRequest.customerId}
            WHERE invoice_id = #{id}
            RETURNING invoice_tb;
            """)
    @ResultMap("InvoiceMap")
    Integer updateInvoiceById(@Param("invoiceRequest") InvoiceRequest invoiceRequest, Integer id);
    @Select("""
            DELETE
            FROM invoice_detail_tb
            WHERE invoice_id = #{invoiceId};
            """)
    void deleteIntoProductDetail(Integer invoiceId);

    @Select("""
            INSERT INTO invoice_detail_tb (invoice_id, product_id)
            VALUES (#{invoiceId}, #{productId});
            """)
    void insertUpdateIntoProductDetail(Integer invoiceId, Integer productId);


    // ========== INSERT A NEW INVOICE ==========
    @Select("""
            INSERT INTO invoice_tb (customer_id) VALUES(#{invoiceRequest.customerId})
            RETURNING invoice_id;
            """)
    Integer insertInvoice(@Param("invoiceRequest") InvoiceRequest invoiceRequest);
    @Select("""
            INSERT INTO invoice_detail_tb (invoice_id, product_id)
            VALUES (#{invoiceId}, #{productId});
            """)
    void insertIntoProductDetail(Integer invoiceId, Integer productId);


    // ========== DELETE INVOICE ==========
    @Select("""
            DELETE
            FROM invoice_tb
            WHERE invoice_id = #{id};
            """)
    Invoice deleteInvoiceById(Integer id);

}
