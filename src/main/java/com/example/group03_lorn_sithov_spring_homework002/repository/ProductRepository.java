package com.example.group03_lorn_sithov_spring_homework002.repository;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Product;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
@Repository
public interface ProductRepository {

    @Select("""
            SELECT *
            FROM product_tb;
            """)
    @Results(id = "productMap", value = {
            @Result(property = "productId", column = "product_id"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productPrice", column = "product_price")
    })
    List<Product> getAllProduct();

    @Select("""
            SELECT *
            FROM product_tb
            WHERE product_id = #{id};
            """)
    @ResultMap("productMap")
    Product getProductById(@PathVariable Integer id);

    @Select("""
            INSERT INTO product_tb (product_name, product_price)
            VALUES (#{product.productName}, #{product.productPrice})
            RETURNING *;
            """)
    @ResultMap("productMap")
    Product addNewProduct(@Param("product") Product product);

    @Select("""
            UPDATE product_tb
            SET product_name  = #{product.productName},
                product_price = #{product.productPrice}
            WHERE product_id = #{id}
            RETURNING *;
            """)
    @ResultMap("productMap")
    Product updateProduct(@Param("product") Product product, @PathVariable Integer id);


    @Select("""
            DELETE
            FROM product_tb
            WHERE product_id = #{id};
            """)
    @ResultMap("productMap")
    Product deleteProduct(Integer id);


    //get products by invoice id
    @Select("""
            SELECT pt.product_id, pt.product_name, pt.product_price
            FROM product_tb pt
                     INNER JOIN invoice_detail_tb pdt ON pt.product_id = pdt.product_id
            WHERE invoice_id = #{invoiceId};
            """)
    @ResultMap("productMap")
    List<Product> getAllInvoiceById(@PathVariable Integer invoiceId);
}
