package com.example.group03_lorn_sithov_spring_homework002.model.request;

import com.example.group03_lorn_sithov_spring_homework002.model.entity.Customer;
import com.example.group03_lorn_sithov_spring_homework002.model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceRequest {

    private Timestamp invoiceDate;
    private Integer customerId;
    private List<Integer> productId;
}
