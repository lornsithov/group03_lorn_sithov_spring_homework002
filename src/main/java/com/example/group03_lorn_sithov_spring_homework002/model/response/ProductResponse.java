package com.example.group03_lorn_sithov_spring_homework002.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse <T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private Boolean success;
    private String message;

}
