package com.example.group03_lorn_sithov_spring_homework002.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer invoiceId;
    private LocalDateTime invoiceDate;
    private Customer customerId;
    private List<Product> products;
}
